﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ExampleSimulation.Data;
using FrameworkInterface.Player;

namespace RandomPlayer
{
	public class RandomPlayer : IPlayer
	{
		Random mRNG = new Random();

		public RandomPlayer() { }

		public override string GetPlayerName()
		{
			return "Random player";
		}

		public override IPlayerOutput OnGameStart(IPlayerInput _input)
		{
			return null;
		}

		public override IPlayerOutput Update(IPlayerInput _input)
		{
			ExamplePlayerOutput output = new ExamplePlayerOutput();

			output.move = (ExamplePlayerOutput.Movement_e)mRNG.Next(0, (int)ExamplePlayerOutput.Movement_e.kMovement_Count);
			return output;
		}

		public override IPlayerOutput OnGameEnd(IPlayerInput _input)
		{
			return null;
		}
	}
}
