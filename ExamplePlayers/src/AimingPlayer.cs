﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ExampleSimulation.Data;
using FrameworkInterface.Player;

namespace AimingPlayer
{
	public class AimingPlayer : IPlayer
	{

		public AimingPlayer() { }

		public override string GetPlayerName()
		{
			return "Aiming player";
		}

		public override IPlayerOutput OnGameStart(IPlayerInput _input)
		{
			return null;
		}

		public override IPlayerOutput Update(IPlayerInput _input)
		{
			ExamplePlayerInput simInput = _input as ExamplePlayerInput;
			if (simInput == null)
				return null;

			// Find my position
			ExamplePlayerInput.PlayerData myData = new ExamplePlayerInput.PlayerData();
			foreach (ExamplePlayerInput.PlayerData curPlayerData in simInput.players)
			{
				if (curPlayerData.uiPlayerIndex != PlayerIndex)
					continue;

				myData = curPlayerData;
				break;
			}
			
			
			// Find the closest, hight point
			Int32 iBestX = 0;
			Int32 iBestY = 0;
			FindBestPoint(myData.iX, myData.iY, simInput.fTerrainHeight, out iBestX, out iBestY);

			// Determine in which direction to move
			ExamplePlayerOutput toReturn = new ExamplePlayerOutput();
			Int32 iDistX = iBestX - myData.iX;
			Int32 iDistY = iBestY - myData.iY;

			if (Math.Abs(iDistX) >= Math.Abs(iDistY))
			{
				if (iDistX == 0)
					toReturn.move = ExamplePlayerOutput.Movement_e.kMovement_None;
				else
					toReturn.move = (iDistX < 0) ? ExamplePlayerOutput.Movement_e.kMovement_Left : ExamplePlayerOutput.Movement_e.kMovement_Right;
			}
			else
			{
				toReturn.move = (iDistY < 0) ? ExamplePlayerOutput.Movement_e.kMovement_Up : ExamplePlayerOutput.Movement_e.kMovement_Down;
			}
			
			return toReturn;
		}

		private void FindBestPoint(Int32 _iCurX, Int32 _iCuY, float[,] _fGrid, out Int32 _iBestX, out Int32 _iBestY)
		{
			_iBestX = 0;
			_iBestY = 0;

			Int32 iWidth  = _fGrid.GetLength(0);
			Int32 iHeight = _fGrid.GetLength(1);

			float fBestHeight   = -1.0f;
			Int32 iBestDistance = 999999999;

			for (Int32 iX = 0; iX < iWidth; ++iX)
			{
				for (Int32 iY = 0; iY < iHeight; ++iY)
				{
					float fValue = _fGrid[iX, iY];

					if (fValue < fBestHeight)
						continue;

					Int32 iDistX = Math.Abs(iX - _iCurX);
					Int32 iDistY = Math.Abs(iY - _iCuY);
					Int32 iTotalDist = iDistX + iDistY;
					
					if (fValue == fBestHeight)
					{
						// Discard distance only if same value
						if (iTotalDist >= iBestDistance)
							continue;
					}

					iBestDistance = iTotalDist;
					fBestHeight   = fValue;

					_iBestX = iX;
					_iBestY = iY;
				}
			}
			
		}

		public override IPlayerOutput OnGameEnd(IPlayerInput _input)
		{
			return null;
		}
	}
}
