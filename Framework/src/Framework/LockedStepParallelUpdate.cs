﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

using FrameworkInterface.Simulation;
using FrameworkInterface.Player;
using FrameworkInterface.Renderer;

namespace Framework
{
	class LockedStepParallelUpdater : IUpdater
	{
		FrameworkInterface.Simulation.LockedStepParallel.ISimulation mSimulation = null;
		IRenderer            mRenderer   = null;
		ICollection<IPlayer> mPlayers    = null;

		public void Update(ISimulation _simulation, IRenderer _renderer, ICollection<IPlayer> _playerCollection)
		{
			mSimulation = _simulation as FrameworkInterface.Simulation.LockedStepParallel.ISimulation;
			mRenderer   = _renderer;
			mPlayers    = _playerCollection;


			// Initialize locks
			Int32        iNumPlayers = mPlayers.Count;
			AutoResetEvent[] turnEvents = new AutoResetEvent[iNumPlayers];
			for (Int32 iCurPlayer = 0; iCurPlayer < iNumPlayers; ++iCurPlayer)
				turnEvents[iCurPlayer] = new AutoResetEvent(false);

			bool bGameFinished = mSimulation.IsGameFinished();
			while (!bGameFinished)
			{

				ICollection<IPlayerInput> playerInputList = new List<IPlayerInput>(mSimulation.OnPlayerTurn_Start(new List<IPlayer>(mPlayers)));
				Debug.Assert(playerInputList.Count == iNumPlayers);

				IEnumerable<Tuple<IPlayer, IPlayerInput>> playerTurnList = mPlayers.Zip(playerInputList, (x, y) => new Tuple<IPlayer, IPlayerInput>(x, y));

				Int32 iCurPlayer = 0;
				List<Tuple<IPlayer, IPlayerOutput>> playerResultList = new List<Tuple<IPlayer, IPlayerOutput>>();
				foreach(Tuple<IPlayer, IPlayerInput> curPlayerTurn in playerTurnList)
				{
					ThreadPool.QueueUserWorkItem(
						(o) =>
							{
								Tuple<IPlayer, IPlayerInput> capturedPlayer = ((object[])o)[0] as Tuple<IPlayer, IPlayerInput>;

								IPlayer      curplayer   = capturedPlayer.Item1;
								IPlayerInput playerInput = capturedPlayer.Item2;
								if (playerInput != null)
									playerInput = playerInput.CopyDeep();

								IPlayerOutput playerOutput = curplayer.Update(playerInput);
								if (playerOutput != null)
									playerOutput = playerOutput.CopyDeep();

								Tuple<IPlayer, IPlayerOutput> result = new Tuple<IPlayer, IPlayerOutput>(curplayer, playerOutput);

								lock (playerResultList)
								{
									playerResultList.Add(result);
								}

								Int32 iPlayerIndex = (Int32)((object[])o)[1];
								turnEvents[iPlayerIndex].Set();
							},

							new object[] { curPlayerTurn, iCurPlayer }
						);

					++iCurPlayer;
				}

				// Wait for everyone to finish
				WaitHandle.WaitAll(turnEvents);

				// Ends the turn, notify simulation and renderer
				ISimulationOutput simOutput = mSimulation.OnPlayerTurn_End(playerResultList);
				if (simOutput != null)
					simOutput = simOutput.CopyDeep();

				IRendererFeedback feedback = mRenderer.Update(simOutput);
				if (feedback != null)
					feedback = feedback.CopyDeep();

				mSimulation.ApplyRendererFeedback(feedback);

				bGameFinished = mSimulation.IsGameFinished();
			}

			// Game finishes
		}
	}
}
