﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkInterface.Renderer
{
	public interface IRendererFeedback
	{
		IRendererFeedback CopyDeep();
	}
}
