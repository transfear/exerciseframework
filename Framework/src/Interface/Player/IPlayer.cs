﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkInterface.Player
{
	public abstract class IPlayer
	{
		// Hidden to users
		private string mDLLPath = null;
		private UInt32 muiIdx   = 0;

		internal void SetDLLPath(string _strPath) { mDLLPath = _strPath; }
		internal void SetPlayerIndex(UInt32 _uiIdx) { muiIdx = _uiIdx; }

		// Base data
		public string DLLPath     { get { return mDLLPath; } }
		public UInt32 PlayerIndex { get { return muiIdx; } }

		// Implementation-specific
		public abstract string        GetPlayerName();
		public abstract IPlayerOutput OnGameStart(IPlayerInput _input);
		public abstract IPlayerOutput OnGameEnd(IPlayerInput _input);

		public abstract IPlayerOutput Update(IPlayerInput _input);
	}
}
