﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using ExampleSimulation.Data;

using FrameworkInterface.Player;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace ExampleSimulation.Internal
{
	class BaseSimulation
	{
		#region Public Interface
		public ICollection<IPlayer> PlayerList    { get; set; }
		public IPlayer              CurrentPlayer { get; set; }

		public void OnGameStart(ICollection<IPlayer> _playerCollection)
		{
			PlayerList = _playerCollection;
			Init();
		}

		public IPlayerInput ProducePlayerInput(IPlayer _player)
		{
			Int32 iNumPlayers = PlayerList.Count;

			ExamplePlayerInput toReturn = new ExamplePlayerInput();
			toReturn.players = new ExamplePlayerInput.PlayerData[iNumPlayers];

			Int32 iCurPlayer = 0;
			foreach (ExampleSimulationOutput.PlayerData players in mPlayerMap.Values)
			{
				toReturn.players[iCurPlayer].uiPlayerIndex = players.player.PlayerIndex;
				toReturn.players[iCurPlayer].iX = players.curPos.iX;
				toReturn.players[iCurPlayer].iY = players.curPos.iY;

				++iCurPlayer;
			}

			toReturn.fTerrainHeight = mfPerlinGrid;
			return toReturn;
		}

		public void ApplyPlayerOuput(IPlayer _player, IPlayerOutput _playerResponse)
		{
			ExamplePlayerOutput response = _playerResponse as ExamplePlayerOutput;
			if (response == null)
				return;
			
			int iMoveX = 0;
			int iMoveY = 0;
			if (response.move == ExamplePlayerOutput.Movement_e.kMovement_Left)
				iMoveX = -1;
			else if (response.move == ExamplePlayerOutput.Movement_e.kMovement_Right)
				iMoveX = 1;
			else if (response.move == ExamplePlayerOutput.Movement_e.kMovement_Up)
				iMoveY = -1;
			else if (response.move == ExamplePlayerOutput.Movement_e.kMovement_Down)
				iMoveY = 1;

			ExampleSimulationOutput.PlayerData playerData = mPlayerMap[_player];

			// Update trail
			if (response.move == ExamplePlayerOutput.Movement_e.kMovement_None)
			{
				if (playerData.trail.Count > 0)
					playerData.trail.RemoveAt(playerData.trail.Count - 1);
			}
			else
			{
				if (playerData.trail.Count == miMaxTrailLength)
					playerData.trail.RemoveAt(miMaxTrailLength - 1);
				playerData.trail.Insert(0, playerData.curPos.CopyDeep());
			}

			// Update pos
			playerData.curPos.iX += iMoveX;
			playerData.curPos.iY += iMoveY;

			if (playerData.curPos.iX < 0)
				playerData.curPos.iX = 0;
			else if (playerData.curPos.iX >= miGridWidth)
				playerData.curPos.iX = miGridWidth - 1;

			if (playerData.curPos.iY < 0)
				playerData.curPos.iY = 0;
			else if (playerData.curPos.iY >= miGridHeight)
				playerData.curPos.iY = miGridHeight - 1;
		}

		public ISimulationOutput ProduceSimulationOutput()
		{
			UpdateTurn();

			ExampleSimulationOutput toReturn = new ExampleSimulationOutput();
			toReturn.players        = mPlayerMap.Values.ToArray();
			toReturn.fTerrainHeight = mfPerlinGrid;			
			toReturn.uiCurrentTurn  = (UInt32)miCurrentTurn;
			toReturn.uiMaxTurns     = (UInt32)miLastTurn;
			toReturn.fMaxScore      = mfMaxScore;

			return toReturn;
		}

		public void ApplyRendererFeedback(IRendererFeedback _rendererFeedback)
		{
			ExampleRendererFeedback feedback = _rendererFeedback as ExampleRendererFeedback;
			if (feedback == null)
				return;

			// Just check if the renderer wants us to quit
			if (feedback.ShouldQuit)
			{
				mbGameFinished  = true;
				mbShouldRestart = feedback.ShouldRestart;
			}
		}

		public void OnGameEnd()
		{
			// Nothing to do here for now
		}

		public bool IsGameFinished()
		{
			return mbGameFinished;
		}

		public bool ShouldRestart()
		{
			return mbShouldRestart;
		}
		#endregion


		#region Private Interface

		Dictionary<IPlayer, ExampleSimulationOutput.PlayerData> mPlayerMap = new Dictionary<IPlayer, ExampleSimulationOutput.PlayerData>();
		bool mbGameFinished  = false;
		bool mbShouldRestart = false;
		
		Int32 miCurrentTurn          = 0;
		Int32 miLastTurn             = 10000;
		float mfMaxScore             = 1000;
		float mfLayerSpeedMultiplier = 1.3f;
		float mfMaxLayerSpeed        = 0.5f;
		Int32 miMaxTrailLength       = 32;

		Int32 miGridWidth   = 256;
		Int32 miGridHeight  = 192;

		Random mRNG = new Random();
		
		// Perlin parameters
		Int32   miNumLayers    = 0;    // any power of two between 8 and 64
		float[] mfLayerSpeedX  = null; // -1 to 1
		float[] mfLayerSpeedY  = null; // -1 to 1

		float[] mfLayerOffsetX       = null;
		float[] mfLayerOffsetY       = null;
		float[] mfOriginalNoiseArray = null;   // 0 to 1

		float[,] mfPerlinGrid = null;
		
		void Init()
		{
			miCurrentTurn  = 0;
			mbGameFinished = false;
			
			// Fill player map
			mPlayerMap.Clear();
			foreach (IPlayer curPlayer in PlayerList)
			{
				ExampleSimulationOutput.PlayerData newData = new ExampleSimulationOutput.PlayerData();

				newData.player = curPlayer;
				newData.color  = GetRandomHue();

				newData.curPos.iX = mRNG.Next(0, miGridWidth);
				newData.curPos.iY = mRNG.Next(0, miGridHeight);

				mPlayerMap.Add(curPlayer, newData);
			}

			// Init perlin noise
			miNumLayers = mRNG.Next(3, 8);

			mfLayerSpeedX  = new float[miNumLayers];
			mfLayerSpeedY  = new float[miNumLayers];
			mfLayerOffsetX = new float[miNumLayers];
			mfLayerOffsetY = new float[miNumLayers];

			for (Int32 iCurLayer = 0; iCurLayer < miNumLayers; ++iCurLayer)
			{
				float fSpeedMultiplier = mfLayerSpeedMultiplier / (float)(miNumLayers - iCurLayer);

				float fLayerSpeedX = ((float)mRNG.NextDouble() * 2.0f - 1.0f) * fSpeedMultiplier;
				float fLayerSpeedY = ((float)mRNG.NextDouble() * 2.0f - 1.0f) * fSpeedMultiplier;

				// Clamp layer speeds
				if (fLayerSpeedX < -mfMaxLayerSpeed)
					fLayerSpeedX = -mfMaxLayerSpeed;
				else if (fLayerSpeedX > mfMaxLayerSpeed)
					fLayerSpeedX = mfMaxLayerSpeed;

				if (fLayerSpeedY < -mfMaxLayerSpeed)
					fLayerSpeedY = -mfMaxLayerSpeed;
				else if (fLayerSpeedY > mfMaxLayerSpeed)
					fLayerSpeedY = mfMaxLayerSpeed;

				mfLayerSpeedX[iCurLayer] = fLayerSpeedX;
				mfLayerSpeedY[iCurLayer] = fLayerSpeedY;

				mfLayerOffsetX[iCurLayer] = (float)mRNG.NextDouble() * miGridWidth;
				mfLayerOffsetY[iCurLayer] = (float)mRNG.NextDouble() * miGridHeight;
			}
			
			Int32 iNumNoiseEntries = miGridWidth * miGridHeight;
			mfOriginalNoiseArray = new float[iNumNoiseEntries];
			for (Int32 iCurNoiseEntry = 0; iCurNoiseEntry < iNumNoiseEntries; ++iCurNoiseEntry)
				mfOriginalNoiseArray[iCurNoiseEntry] = (float)mRNG.NextDouble();

			mfPerlinGrid = new float[miGridWidth, miGridHeight];
			UpdatePerlin();
		}

		Color GetRandomHue()
		{
			float fRandomHue = (float)mRNG.NextDouble();
			float fR = 0.0f;
			float fG = 0.0f;
			float fB = 0.0f;

			const float fOneThird = 1.0f / 3.0f;
			const float fTwoThirds = 2.0f / 3.0f;

			if (fRandomHue < fOneThird)
			{
				float fLerp = fRandomHue / fOneThird;
				if (fLerp < 0.5f)
				{
					fLerp *= 2.0f;
					fR     = 1.0f;
					fG     = Math.Max(fG, fLerp);
				}
				else
				{
					fLerp = (fLerp - 0.5f) * 2.0f;
					fG    = 1.0f;
					fR    = Math.Max(fR, 1.0f - fLerp);
				}

			}
			else if (fRandomHue < fTwoThirds)
			{
				fRandomHue -= fOneThird;
				float fLerp = fRandomHue / fOneThird;
				if (fLerp < 0.5f)
				{
					fLerp *= 2.0f;
					fG     = 1.0f;
					fB     = Math.Max(fB, fLerp);
				}
				else
				{
					fLerp = (fLerp - 0.5f) * 2.0f;
					fB    = 1.0f;
					fG    = Math.Max(fG, 1.0f - fLerp);
				}
			}
			else
			{
				fRandomHue -= fTwoThirds;
				float fLerp = fRandomHue / fOneThird;
				if (fLerp < 0.5f)
				{
					fLerp *= 2.0f;
					fB     = 1.0f;
					fR     = Math.Max(fR, fLerp);
				}
				else
				{
					fLerp = (fLerp - 0.5f) * 2.0f;
					fR    = 1.0f;
					fB    = Math.Max(fB, 1.0f - fLerp);
				}
			}


			return Color.FromArgb(255, (byte)(fR * 255.0f), (byte)(fG * 255.0f), (byte)(fB * 255.0f));
		}

		void UpdateTurn()
		{
			// Update perlin layer offsets and grid height
			for (Int32 iCurLayer = 0; iCurLayer < miNumLayers; ++iCurLayer )
			{
				mfLayerOffsetX[iCurLayer] += mfLayerSpeedX[iCurLayer];
				mfLayerOffsetY[iCurLayer] += mfLayerSpeedY[iCurLayer];
			}

			UpdatePerlin();

			// Update player score
			foreach (ExampleSimulationOutput.PlayerData curPlayerData in mPlayerMap.Values)
			{
				int iX = curPlayerData.curPos.iX;
				int iY = curPlayerData.curPos.iY;
				
				float fHeight = mfPerlinGrid[iX, iY];
				curPlayerData.fScore += fHeight;
			}
			
			++miCurrentTurn;

			// Check if game is finished
			if (miCurrentTurn >= miLastTurn)
			{
				mbGameFinished = true;
			}
			else
			{
				foreach (ExampleSimulationOutput.PlayerData curData in mPlayerMap.Values)
				{
					if (curData.fScore >= mfMaxScore)
					{
						mbGameFinished = true;
						break;
					}
				}
			}
		}

		void UpdatePerlin()
		{
			float fInitialSize = (float)(1 << miNumLayers);
			float fMaxValue = fInitialSize * 2.0f - 1.0f;
			
			for (Int32 iX = 0; iX < miGridWidth; ++iX)
			{
				for (Int32 iY = 0; iY < miGridHeight; ++iY)
				{
					float fSize   = fInitialSize;
					float fValue = 0.0f;

					Int32 iCurLayer = 0;
					while (iCurLayer < miNumLayers)
					{
						// Find current offset to sample
						float fXValue = (((float)iX + mfLayerOffsetX[iCurLayer]) / fSize) % (float)miGridWidth;
						if (fXValue < 0.0f)
							fXValue += miGridWidth;

						float fYValue = (((float)iY + mfLayerOffsetY[iCurLayer]) / fSize) % (float)miGridHeight;
						if (fYValue < 0.0f)
							fYValue += miGridHeight;

						float fFracX = fXValue - (float)Math.Truncate(fXValue);
						float fFracY = fYValue - (float)Math.Truncate(fYValue);

						// First sample coords
						Int32 iX1 = ((Int32)fXValue + miGridWidth ) % miGridWidth;
						Int32 iY1 = ((Int32)fYValue + miGridHeight) % miGridHeight;
   
						// Second sample coords
						Int32 iX2 = (iX1 + miGridWidth  - 1) % miGridWidth;
						Int32 iY2 = (iY1 + miGridHeight - 1) % miGridHeight;

						// Smoothing
						float fLocalNoise  = fFracX          *         fFracY  * mfOriginalNoiseArray[iX1 * miGridHeight + iY1];
						      fLocalNoise += fFracX          * (1.0f - fFracY) * mfOriginalNoiseArray[iX1 * miGridHeight + iY2];
						      fLocalNoise += (1.0f - fFracX) *         fFracY  * mfOriginalNoiseArray[iX2 * miGridHeight + iY1];
							  fLocalNoise += (1.0f - fFracX) * (1.0f - fFracY) * mfOriginalNoiseArray[iX2 * miGridHeight + iY2];
						
						fValue += fLocalNoise * fSize;
						
						fSize *= 0.5f;
						++iCurLayer;
					}

					float fFinalValue = fValue / fMaxValue;
					mfPerlinGrid[iX, iY] = fFinalValue;
				}
			}
		}
		
		#endregion
	}
}
