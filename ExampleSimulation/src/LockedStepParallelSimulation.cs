﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace ExampleSimulation
{
	public class LockedStepParallelSimulation : FrameworkInterface.Simulation.LockedStepParallel.ISimulation
	{
		Internal.BaseSimulation mSimulation = new Internal.BaseSimulation();

		public LockedStepParallelSimulation() { }

		#region From LockedStepParallel.ISimulation interface

		public override ICollection<IPlayerInput> OnGameStart_Begin(ICollection<IPlayer> _playerCollection)
		{
			mSimulation.OnGameStart(_playerCollection);

			List<IPlayerInput> playerInputList = new List<IPlayerInput>();
			foreach (IPlayer curPlayer in _playerCollection)
			{
				IPlayerInput curPlayerInput = mSimulation.ProducePlayerInput(curPlayer);
				playerInputList.Add(curPlayerInput);
			}

			return playerInputList;
		}

		public override FrameworkInterface.Simulation.ISimulationOutput OnGameStart_End(ICollection<IPlayerOutput> _playersResponse)
		{
			// Ignore player responses for now

			return mSimulation.ProduceSimulationOutput();
		}

		public override ICollection<IPlayerInput> OnPlayerTurn_Start(ICollection<IPlayer> _playerCollection)
		{
			List<IPlayerInput> playerInputList = new List<IPlayerInput>();
			foreach (IPlayer curPlayer in _playerCollection)
			{
				IPlayerInput curPlayerInput = mSimulation.ProducePlayerInput(curPlayer);
				playerInputList.Add(curPlayerInput);
			}

			return playerInputList;
		}

		public override FrameworkInterface.Simulation.ISimulationOutput OnPlayerTurn_End(ICollection<Tuple<IPlayer, IPlayerOutput>> _playerOutputs)
		{
			// Apply each player's feedback
			foreach (Tuple<IPlayer, IPlayerOutput> playerResponse in _playerOutputs)
			{
				mSimulation.ApplyPlayerOuput(playerResponse.Item1, playerResponse.Item2);
			}

			return mSimulation.ProduceSimulationOutput();
		}

		public override void ApplyRendererFeedback(FrameworkInterface.Renderer.IRendererFeedback _rendererFeedback)
		{
			mSimulation.ApplyRendererFeedback(_rendererFeedback);
		}

		public override ICollection<IPlayerInput> OnGameEnd_Begin(ICollection<IPlayer> _playerCollection)
		{
			List<IPlayerInput> playerInputList = new List<IPlayerInput>();
			foreach (IPlayer curPlayer in _playerCollection)
			{
				IPlayerInput curPlayerInput = mSimulation.ProducePlayerInput(curPlayer);
				playerInputList.Add(curPlayerInput);
			}

			mSimulation.OnGameEnd();

			return playerInputList;
		}

		public override FrameworkInterface.Simulation.ISimulationOutput OnGameEnd_End(ICollection<IPlayerOutput> _playersResponse)
		{
			return mSimulation.ProduceSimulationOutput();
		}

		public override bool IsGameFinished()
		{
			return mSimulation.IsGameFinished();
		}

		public override bool ShouldRestart()
		{
			return mSimulation.ShouldRestart();
		}
		#endregion
	}
}
