﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExampleSimulation.Instantiator
{
	/// <summary>
	/// Interaction logic for SimulationSelector.xaml
	/// </summary>
	public partial class SimulationSelector : Window
	{
		List<Type> mAvailableTypes = null;
		Type       mSelectedType   = null;
		
		public SimulationSelector(List<Type> _typeList)
		{
			InitializeComponent();

			mAvailableTypes = new List<Type>(_typeList);

			foreach (Type availableType in mAvailableTypes)
			{
				ComboBoxItem newItem = new ComboBoxItem();
				newItem.Content = availableType.Name;

				cboSimulations.Items.Add(newItem);
			}

			if (mAvailableTypes.Count > 0)
				cboSimulations.SelectedIndex = 0;
		}

		public Type GetSelectedType()
		{
			return mSelectedType;
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			Int32 iSelectedIdx = cboSimulations.SelectedIndex;
			mSelectedType = mAvailableTypes.ElementAt(iSelectedIdx);

			Close();
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			mSelectedType = null;

			Close();
		}
	}
}
