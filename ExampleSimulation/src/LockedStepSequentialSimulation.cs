﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace ExampleSimulation
{
	public class LockedStepSequentialSimulation : FrameworkInterface.Simulation.LockedStepSequential.ISimulation
	{
		Internal.BaseSimulation mSimulation = new Internal.BaseSimulation();

		public LockedStepSequentialSimulation() { }

		#region From LockedStepSequential.ISimulation interface
		public override ICollection<IPlayer> GetPlayerOrder(ICollection<IPlayer> _playerList)
		{
			// Randomize order
			List<IPlayer> shuffledList = new List<IPlayer>(_playerList);
			
			ShuffleList(shuffledList);
			return shuffledList;
		}

		public override ICollection<IPlayerInput> OnGameStart_Begin(ICollection<IPlayer> _playerCollection)
		{
			mSimulation.OnGameStart(_playerCollection);

			List<IPlayerInput> playerInputList = new List<IPlayerInput>();
			foreach (IPlayer curPlayer in _playerCollection)
			{
				IPlayerInput curPlayerInput = mSimulation.ProducePlayerInput(curPlayer);
				playerInputList.Add(curPlayerInput);
			}

			return playerInputList;
		}

		public override FrameworkInterface.Simulation.ISimulationOutput OnGameStart_End(ICollection<IPlayerOutput> _playersResponse)
		{
			// Ignore player responses for now

			return mSimulation.ProduceSimulationOutput();
		}

		public override IPlayerInput OnPlayerTurn_Start(IPlayer _playerToUpdate)
		{
			return mSimulation.ProducePlayerInput(_playerToUpdate);
		}

		public override FrameworkInterface.Simulation.ISimulationOutput OnPlayerTurn_End(IPlayer _updatedPlayer, IPlayerOutput _playerResponse)
		{
			mSimulation.ApplyPlayerOuput(_updatedPlayer, _playerResponse);
			return mSimulation.ProduceSimulationOutput();
		}

		public override void ApplyRendererFeedback(FrameworkInterface.Renderer.IRendererFeedback _rendererFeedback)
		{
			mSimulation.ApplyRendererFeedback(_rendererFeedback);
		}

		public override ICollection<IPlayerInput> OnGameEnd_Begin(ICollection<IPlayer> _playerCollection)
		{
			List<IPlayerInput> playerInputList = new List<IPlayerInput>();
			foreach (IPlayer curPlayer in _playerCollection)
			{
				IPlayerInput curPlayerInput = mSimulation.ProducePlayerInput(curPlayer);
				playerInputList.Add(curPlayerInput);
			}

			mSimulation.OnGameEnd();

			return playerInputList;
		}

		public override FrameworkInterface.Simulation.ISimulationOutput OnGameEnd_End(ICollection<IPlayerOutput> _playersResponse)
		{
			return mSimulation.ProduceSimulationOutput();
		}

		public override bool IsGameFinished()
		{
			return mSimulation.IsGameFinished();
		}

		public override bool ShouldRestart()
		{
			return mSimulation.ShouldRestart();
		}
		#endregion

		private void ShuffleList(List<IPlayer> _inputList)
		{
			Random randGen            = new Random();
			Int32  iRemainingElements = _inputList.Count();
			while (iRemainingElements > 1)
			{
				--iRemainingElements;

				Int32 iToSwap = randGen.Next(iRemainingElements + 1);

				IPlayer playerToSwap           = _inputList[iToSwap];
				_inputList[iToSwap]            = _inputList[iRemainingElements];
				_inputList[iRemainingElements] = playerToSwap;
			}
		}
	}
}
