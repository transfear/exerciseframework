﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using FrameworkInterface.Simulation;

namespace ExampleSimulation.Data
{
	public class ExampleSimulationOutput : ISimulationOutput
	{
		public class Coord2
		{
			public int iX;
			public int iY;

			public Coord2 CopyDeep()
			{
				Coord2 copy = new Coord2();
				copy.iX = iX;
				copy.iY = iY;
				return copy;
			}
		};

		public class PlayerData
		{
			public FrameworkInterface.Player.IPlayer player = null;

			public float        fScore = 0.0f;
			public Color        color;
			public Coord2       curPos = new Coord2();
			public List<Coord2> trail  = new List<Coord2>();

			public PlayerData CopyDeep()
			{
				PlayerData copy = new PlayerData();
				copy.player = player;
				copy.fScore = fScore;
				copy.color  = color;
				copy.curPos = curPos.CopyDeep();

				foreach (Coord2 curTrail in trail)
					copy.trail.Add(curTrail.CopyDeep());

				return copy;
			}
			
		};

		public PlayerData[] players;
		public float[,]     fTerrainHeight;	// Between 0.0f and 1.0f
		public UInt32       uiCurrentTurn = 0;
		public UInt32       uiMaxTurns    = 0;
		public float        fMaxScore     = 0;

		// From ISimulationOutput interface
		public ISimulationOutput CopyDeep()
		{
			ExampleSimulationOutput copy = new ExampleSimulationOutput();

			// Copy players
			Int32 iNumPlayers = players.GetLength(0);
			copy.players = new PlayerData[iNumPlayers];
			for (Int32 iCurPlayer = 0; iCurPlayer < iNumPlayers; ++iCurPlayer)
				copy.players[iCurPlayer] = players[iCurPlayer].CopyDeep();
			
			// Copy terrain height
			Int32 iNumColumns = fTerrainHeight.GetLength(0);
			Int32 iNumRows    = fTerrainHeight.GetLength(1);
			copy.fTerrainHeight = new float[iNumColumns, iNumRows];
			Array.Copy(fTerrainHeight, copy.fTerrainHeight, iNumColumns * iNumRows);

			copy.uiCurrentTurn = uiCurrentTurn;
			copy.uiMaxTurns    = uiMaxTurns;
			copy.fMaxScore     = fMaxScore;

			return copy;
		}
	}
}
