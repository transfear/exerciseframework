﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace ExampleSimulation.Data
{
	public class ExamplePlayerInput : IPlayerInput
	{
		public struct PlayerData
		{
			public UInt32 uiPlayerIndex;
			public int iX;
			public int iY;
		};
		
		public PlayerData[]  players;
		public float[,]      fTerrainHeight;	// Between 0.0f and 1.0f

		// From IPlayerInputInterface
		public IPlayerInput CopyDeep()
		{
			ExamplePlayerInput copy = new ExamplePlayerInput();

			// Copy players
			Int32 iNumPlayers = players.GetLength(0);
			copy.players = new PlayerData[iNumPlayers];
			Array.Copy(players, copy.players, iNumPlayers);

			// Copy terrain height
			Int32 iNumColumns = fTerrainHeight.GetLength(0);
			Int32 iNumRows    = fTerrainHeight.GetLength(1);
			copy.fTerrainHeight = new float[iNumColumns, iNumRows];
			Array.Copy(fTerrainHeight, copy.fTerrainHeight, iNumColumns * iNumRows);

			return copy;
		}
	}
}
