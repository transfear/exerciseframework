﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace ExampleSimulation.Data
{
	public class ExamplePlayerOutput : IPlayerOutput
	{
		public enum Movement_e
		{
			kMovement_Up = 0,
			kMovement_Right,
			kMovement_Down,
			kMovement_Left,
			kMovement_None,

			kMovement_Count	// Do not use
		};

		public Movement_e move;

		// From IPlayerOutput interface
		public IPlayerOutput CopyDeep()
		{
			ExamplePlayerOutput copy = new ExamplePlayerOutput();
			copy.move = move;
			return copy;
		}
	}
}
