﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Renderer;

namespace ExampleSimulation.Data
{
	public class ExampleRendererFeedback : IRendererFeedback
	{
		public bool ShouldQuit    { get; set; }
		public bool ShouldRestart { get; set; }

		// From IRendererFeedback interface
		public IRendererFeedback CopyDeep()
		{
			ExampleRendererFeedback copy = new ExampleRendererFeedback();
			copy.ShouldQuit    = ShouldQuit;
			copy.ShouldRestart = ShouldRestart;

			return copy;
		}
	}
}
