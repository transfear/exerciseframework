﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using ExampleSimulation.Data;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace ExampleRenderer
{
	public class ExampleRenderer : IRenderer
	{
		AutoResetEvent           mWindowSetEvent = new AutoResetEvent(false);
		UI.ExampleRendererWindow mWindow         = null;
		UIThread                 mUIThread       = null;

		public IRendererFeedback OnGameStart(ISimulationOutput _simulationData)
		{
			ExampleSimulationOutput simOutput = _simulationData as ExampleSimulationOutput;
			if (simOutput == null)
				return null;

			bool bFirstStart = mUIThread == null;
			if (bFirstStart)
			{
				mUIThread = new UIThread(this);
				mUIThread.StartUIThread(simOutput);
				mWindowSetEvent.WaitOne();
			}

			mWindow.CloseRequested = false;
			return null;
		}

		public void SetWindow(UI.ExampleRendererWindow _window)
		{
			mWindow = _window;
			mWindowSetEvent.Set();
		}

		public IRendererFeedback Update(ISimulationOutput _simulationData)
		{
			ExampleSimulationOutput simOutput = _simulationData as ExampleSimulationOutput;
			if (simOutput == null)
				return null;

			// Create feedback for the simulation
			ExampleRendererFeedback feedback = new ExampleRendererFeedback();
			feedback.ShouldQuit = false;

			if (mWindow != null)
			{
				feedback.ShouldQuit    = mWindow.CloseRequested;
				feedback.ShouldRestart = false;

				mWindow.Dispatcher.BeginInvoke((Action)(() => { mWindow.UpdateUI(simOutput); }));				
			}

			return feedback;
		}

		public IRendererFeedback OnGameEnd(ISimulationOutput _simulationData)
		{
			ExampleRendererFeedback newFeedBack = new ExampleRendererFeedback();
			newFeedBack.ShouldQuit    = true;
			newFeedBack.ShouldRestart = false;

			ExampleSimulationOutput simOutput = _simulationData as ExampleSimulationOutput;
			if (simOutput != null)
			{
				// Attempt to find best player
				float fBestScore = -1.0f;
				ExampleSimulationOutput.PlayerData bestPlayer = null;
				foreach (ExampleSimulationOutput.PlayerData curPlayer in simOutput.players)
				{
					if (curPlayer.fScore > fBestScore)
					{
						fBestScore = curPlayer.fScore;
						bestPlayer = curPlayer;
					}
				}

				if (bestPlayer != null)
				{
					UInt32 uiScore = (UInt32)fBestScore;
					MessageBoxResult msgResult =  MessageBox.Show(bestPlayer.player.GetPlayerName() + " won with " + uiScore.ToString() + " points. Would you like to restart?", "Game finished!", MessageBoxButton.YesNo);
					newFeedBack.ShouldRestart = msgResult == MessageBoxResult.Yes;
				}
			}

			// Close the window
			mWindow.Dispatcher.Invoke((Action)(() => 
			{ 
				// Explicit shutdown
				if (!newFeedBack.ShouldRestart)
				{
					mWindow.Close();
					mUIThread.ExplicitShutdown();
				}
			}));

			// Wait for the thread to finish
			if (!newFeedBack.ShouldRestart)
				mUIThread.Join();

			return newFeedBack;
		}
	}
}
