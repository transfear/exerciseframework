﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using FrameworkInterface.Player;
using ExampleSimulation.Data;

namespace ExampleRenderer.UI
{
	/// <summary>
	/// Interaction logic for ExampleRendererWindow.xaml
	/// </summary>
	public partial class ExampleRendererWindow : Window
	{
		private Dictionary<IPlayer, Label> mPlayerScoreboard  = new Dictionary<IPlayer, Label>();
		private System.Windows.Media.Color mScoreBoardLightBgColor = System.Windows.Media.Color.FromArgb(255, 0, 96, 0);
		private System.Windows.Media.Color mScoreBoardDarkBgColor  = System.Windows.Media.Color.FromArgb(255, 0, 32, 0);

		public bool CloseRequested { get; set; }

		public ExampleRendererWindow()
		{
			InitializeComponent();

			CloseRequested = false;
			Closing += OnWindowClosing;
		}

		public void OnGameStart(ExampleSimulationOutput _simOutput)
		{
			mPlayerScoreboard.Clear();
			stckPanelScores.Children.Clear();

			foreach (ExampleSimulationOutput.PlayerData curPlayer in _simOutput.players)
			{
				IPlayer player = curPlayer.player;

				Label playerLbl = new Label();
				playerLbl.Foreground = new System.Windows.Media.SolidColorBrush(
				    System.Windows.Media.Color.FromArgb(
				        curPlayer.color.A, curPlayer.color.R, curPlayer.color.G, curPlayer.color.B)
					);

				stckPanelScores.Children.Add(playerLbl);
				mPlayerScoreboard.Add(player, playerLbl);
			}
		}

		public void UpdateUI(ExampleSimulationOutput _simOutput)
		{
			Int32 iWidth  = _simOutput.fTerrainHeight.GetLength(0);
			Int32 iHeight = _simOutput.fTerrainHeight.GetLength(1);
			
			Int32 iNumPixels   = iWidth * iHeight * 3;
			byte[] pixelData   = new byte[iNumPixels];
			Int32 iCurPixelIdx = 0;

			// Generate terrain
			for (Int32 iY = 0; iY < iHeight; ++iY)
			{
				for (Int32 iX = 0; iX < iWidth; ++iX)
				{
					pixelData[iCurPixelIdx + 0] = (byte)(_simOutput.fTerrainHeight[iX, iY] * 255.0f);
					pixelData[iCurPixelIdx + 1] = pixelData[iCurPixelIdx];
					pixelData[iCurPixelIdx + 2] = pixelData[iCurPixelIdx];
					iCurPixelIdx += 3;
				}
			}

			// Generate players
			foreach (ExampleSimulationOutput.PlayerData curPlayer in _simOutput.players)
			{
				System.Drawing.Color playerColor = curPlayer.color;
				Int32 iPosPixelIndex = (curPlayer.curPos.iX + curPlayer.curPos.iY * iWidth) * 3;

				pixelData[iPosPixelIndex + 0] = playerColor.R;
				pixelData[iPosPixelIndex + 1] = playerColor.G;
				pixelData[iPosPixelIndex + 2] = playerColor.B;

				// Draw trail
				Int32 iTrailLen = curPlayer.trail.Count;
				float fDelta   = 1.0f / (float)(iTrailLen + 1);
				float fAlpha   = 1.0f - fDelta;
				foreach (ExampleSimulationOutput.Coord2 curTrail in curPlayer.trail)
				{
					Int32 iTrailPixelIndex = (curTrail.iX + curTrail.iY * iWidth) * 3;

					float fOriginalR = pixelData[iTrailPixelIndex + 0];
					float fOriginalG = pixelData[iTrailPixelIndex + 1];
					float fOriginalB = pixelData[iTrailPixelIndex + 2];

					pixelData[iTrailPixelIndex + 0] = (byte)(playerColor.R * fAlpha + fOriginalR * (1.0f - fAlpha));
					pixelData[iTrailPixelIndex + 1] = (byte)(playerColor.G * fAlpha + fOriginalG * (1.0f - fAlpha));
					pixelData[iTrailPixelIndex + 2] = (byte)(playerColor.B * fAlpha + fOriginalB * (1.0f - fAlpha));

					fAlpha -= fDelta;
				}

				// Update scoreboard
				IPlayer player     = curPlayer.player;
				Label   scoreBoard = mPlayerScoreboard[player];
				UInt32  uiScore    = (UInt32)curPlayer.fScore;
				scoreBoard.Content = player.GetPlayerName() + ": " + uiScore.ToString() + " points";


				float fScorePct = curPlayer.fScore / _simOutput.fMaxScore;
				GradientStopCollection gsCollection = new GradientStopCollection(new GradientStop[] 
				{
					new GradientStop(mScoreBoardDarkBgColor,   0),
					new GradientStop(mScoreBoardLightBgColor,  fScorePct),
					new GradientStop(Colors.Black,             fScorePct),
					new GradientStop(Colors.Black,             1)
				});
				LinearGradientBrush lgBrush = new LinearGradientBrush(gsCollection, 0);
				scoreBoard.Background = lgBrush;
			}

			lblTurn.Content = "Turn " + _simOutput.uiCurrentTurn.ToString() + " of " + _simOutput.uiMaxTurns.ToString();

			// Update image
			WriteableBitmap wb = new WriteableBitmap(iWidth, iHeight, 96, 96, PixelFormats.Rgb24, null);
			wb.WritePixels(new Int32Rect(0, 0, iWidth, iHeight), pixelData, iWidth * 3, 0);
			imgSimulation.Source = wb;
		}

		private void OnWindowClosing(object sender, CancelEventArgs e)
		{
			// Always prevent the window from closing - let the simulation decide what to do
			CloseRequested = true;
			e.Cancel = true;
		}
	}
}
